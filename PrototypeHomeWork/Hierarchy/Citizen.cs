using System.Threading;
using PrototypeHomeWork.Hierarchy.Core;

namespace PrototypeHomeWork.Hierarchy
{
    public class Citizen : Human
    {
        internal Passport Passport { get; set; }
        
        public Citizen(Passport passport = null, Gender? gender = null):base(gender)
        {
            this.Passport = passport;
        }
        public Citizen(Citizen source):base(source == null ? null : new Human(source?.Gender))
        {
            if (source == null) return;
            this.Passport = (Passport)source.Passport.Clone();
        }
        
        public override Human MyClone()
        {
            return new Citizen(this);
        }

        public override object Clone()
        {
            return MyClone();
        }
    }
}