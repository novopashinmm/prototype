namespace PrototypeHomeWork.Hierarchy.Core
{

    public enum Citizenship
    {
        rf = 0,
        us = 1,
        grm = 2
    }
    
    public enum Gender
    {
        male = 0,
        female = 1
    }
}