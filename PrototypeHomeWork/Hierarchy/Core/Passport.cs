using System;

namespace PrototypeHomeWork.Hierarchy.Core
{
    public class Passport : ICloneable
    {
        public string Number { get; set; }
        public Citizenship Citizenship { get; set; }
        public Passport(string number, Citizenship citizenship)
        {
            Number = number;
            Citizenship = citizenship;
        }

        public object Clone()
        {
            return this.MemberwiseClone();
        }
    }
}