using System;

namespace PrototypeHomeWork.Hierarchy.Core
{
    public class Summary : ICloneable
    {
        public string Info { get; set; }

        public Summary(string info = null)
        {
            this.Info = info;
        }

        public object Clone()
        {
            return this.MemberwiseClone();
        }
    }
}