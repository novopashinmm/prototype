using System.Runtime.CompilerServices;
using PrototypeHomeWork.Hierarchy.Core;

namespace PrototypeHomeWork.Hierarchy
{
    public class Employee : Citizen
    {
        public string CurrentPosition { get; set; }
        public Summary Summary { get; set; }

        public Employee(string currentPosition = null, 
                        Summary summary = null,
                        Passport passport = null,
                        Gender? gender = null): base(passport, gender)
        
        {
            this.CurrentPosition = currentPosition;
            this.Summary = summary;
        }

        public Employee(Employee source):base(source == null ? null : new Citizen(source?.Passport, source?.Gender))
        {
            if (source == null) return;
            this.Summary = (Summary)source.Summary.Clone();
            this.CurrentPosition = source.CurrentPosition;
        }

        public override Human MyClone()
        {
            return new Employee(this);
        }

        public override object Clone()
        {
            return MyClone();
        }
    }
}