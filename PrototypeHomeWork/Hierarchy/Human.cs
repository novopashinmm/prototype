using System;
using System.Security;
using PrototypeHomeWork.Hierarchy.Core;

namespace PrototypeHomeWork.Hierarchy
{
    public class Human : IMyCloneable<Human>, ICloneable
    {
        public Gender? Gender { get; set; }
        
        public Human(Gender? gender = null)
        {
            this.Gender = gender;
        }
        public Human(Human source)
        {
            if (source == null) return;
            this.Gender = source.Gender;
        }
        public virtual Human MyClone()
        {
            return new Human(this);
        }

        public virtual object Clone()
        {
            return MyClone();
        }
    }
}