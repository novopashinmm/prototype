namespace PrototypeHomeWork
{
    public interface IMyCloneable<T>
    {
        T MyClone();
    }
}