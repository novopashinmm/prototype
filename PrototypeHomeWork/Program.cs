﻿using System;
using PrototypeHomeWork.Hierarchy;
using PrototypeHomeWork.Hierarchy.Core;

namespace PrototypeHomeWork
{
    class Program
    {
        static void Main(string[] args)
        {
            TestMyClone();
            TestClone();
        }

        static void TestMyClone()
        {
            var human = new Human(Gender.male);
            var humanClone = human.MyClone();
            human.Gender = Gender.female;
            Console.WriteLine($"Test1\nhuman.gender={human.Gender.ToString()}; humanclone.gender={humanClone.Gender.ToString()}");
            
            var passport = new Passport("666", Citizenship.rf);
            var citizen = new Citizen(passport, Gender.female);
            var citizenClone = (Citizen)citizen.MyClone();
            citizen.Passport.Number = "777";
            citizen.Passport.Citizenship = Citizenship.grm;
            Console.WriteLine($"Test2\ncitizen.passportNumber={citizen.Passport.Number}; citizenClone.passportNumber={citizenClone.Passport.Number}");
            
            var summary = new Summary("Programmer");
            var employee = new Employee("Programmer", summary, passport, Gender.female);
            var employeeClone = (Employee)employee.MyClone();
            employee.Passport.Number = "888";
            employee.Summary.Info += " Worked in Google crp.";
            Console.WriteLine($"Test3\nemployee.passportNumber={employee.Passport.Number}, employee.Summary.Info={employee.Summary.Info};");
            Console.WriteLine($"employeeClone.passportNumber={employeeClone.Passport.Number}, employeeClone.Summary.Info={employeeClone.Summary.Info};");
        }

        static void TestClone()
        {
            var human = new Human(Gender.male);
            var humanClone = (Human)human.Clone();
            human.Gender = Gender.female;
            Console.WriteLine($"Test4\nhuman.gender={human.Gender.ToString()}; humanclone.gender={humanClone.Gender.ToString()}");
            
            var passport = new Passport("666", Citizenship.rf);
            var citizen = new Citizen(passport, Gender.female);
            var citizenClone = (Citizen)citizen.Clone();
            citizen.Passport.Number = "777";
            citizen.Passport.Citizenship = Citizenship.grm;
            Console.WriteLine($"Test5\ncitizen.passportNumber={citizen.Passport.Number}; citizenClone.passportNumber={citizenClone.Passport.Number}");
            
            var summary = new Summary("Programmer");
            var employee = new Employee("Programmer", summary, passport, Gender.female);
            var employeeClone = (Employee)employee.Clone();
            employee.Passport.Number = "888";
            employee.Summary.Info += " Worked in Google crp.";
            Console.WriteLine($"Test6\nemployee.passportNumber={employee.Passport.Number}, employee.Summary.Info={employee.Summary.Info};");
            Console.WriteLine($"employeeClone.passportNumber={employeeClone.Passport.Number}, employeeClone.Summary.Info={employeeClone.Summary.Info};");
        }
    }
}
